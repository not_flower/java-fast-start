package com.simple.demo.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

@Api(tags = "用户接口")
@RestController
@RequestMapping("/api/user")
public class UserController {

    @ApiOperation(value = "登录")
    @PostMapping("/login")
    public String login(){
        return "登录成功";
    }

}
