//package com.zd.syzl;
//
//import com.zd.syzl.mapper.lcws.LcwsSbJbxxMapper;
//import com.zd.syzl.mapper.lcws.impl.LcwsSbBjxxImpl;
//import com.zd.syzl.mapper.lcws.impl.LcwsSbClztImpl;
//import com.zd.syzl.mapper.lcws.impl.LcwsSbJbxxImpl;
//import com.zd.syzl.model.lcws.LcwsSbBjxx;
//import com.zd.syzl.model.lcws.LcwsSbClzt;
//import com.zd.syzl.model.lcws.LcwsSbJbxx;
//import com.zd.syzl.utils.SnowFlake;
//import org.junit.jupiter.api.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.List;
//import java.util.Random;
//
//
//@AutoConfigureMockMvc
//@SpringBootTest
//class SyzlApplicationTests {
//
//    @Autowired
//    LcwsSbJbxxImpl lcwsSbJbxxImpl;
//    @Autowired
//    LcwsSbClztImpl lcwsSbClztImpl;
//    @Autowired
//    LcwsSbBjxxImpl lcwsSbBjxxImpl;
//
//    static String[] start = {"133", "149", "153", "173", "177","180", "181", "189", "199", "130", "131", "132", "145", "155", "156", "166", "171", "175", "176", "185", "186", "166", "134", "135", "136", "137", "138", "139", "147", "150", "151", "152", "157", "158", "159", "172", "178", "182", "183", "184", "187", "188", "198", "170", "171"};
//    static String cllx[] = new String[]{"汽车","电瓶车","摩托车","四轮电瓶车"};
//    static String xzqhdm[] = "513323,513325,513327,513329,513331,513333,513335,513321,513322,513324,513326,513328,513330,513332,513334,513336,513337,513338".split(",");
//
//    static String familyName1 = "赵钱孙李周吴郑王冯陈褚卫蒋沈韩杨朱秦尤许何吕施张孔曹严华金魏陶姜戚谢邹喻水云苏潘葛奚范彭郎鲁韦昌马苗凤花方俞任袁柳鲍史唐费岑薛雷贺倪汤滕殷罗毕郝邬安常乐于时傅卞齐康伍余元卜顾孟平"
//            + "黄和穆萧尹姚邵湛汪祁毛禹狄米贝明臧计成戴宋茅庞熊纪舒屈项祝董粱杜阮席季麻强贾路娄危江童颜郭梅盛林刁钟徐邱骆高夏蔡田胡凌霍万柯卢莫房缪干解应宗丁宣邓郁单杭洪包诸左石崔吉"
//            + "龚程邢滑裴陆荣翁荀羊甄家封芮储靳邴松井富乌焦巴弓牧隗山谷车侯伊宁仇祖武符刘景詹束龙叶幸司韶黎乔苍双闻莘劳逄姬冉宰桂牛寿通边燕冀尚农温庄晏瞿茹习鱼容向古戈终居衡步都耿满弘国文东殴沃曾关红游盖益桓公晋楚闫";
//   static String girlName = "秀娟英华慧巧美娜静淑惠珠翠雅芝玉萍红娥玲芬芳燕彩春菊兰凤洁梅琳素云莲真环雪荣爱妹霞香月莺媛艳瑞凡佳嘉琼勤珍贞莉桂娣叶璧璐娅琦晶妍茜秋珊莎锦黛青倩婷姣婉娴瑾颖露瑶怡婵雁蓓纨仪荷丹蓉眉君琴蕊薇菁梦岚苑婕馨瑗琰韵融园艺咏卿聪澜纯毓悦昭冰爽琬茗羽希宁欣飘育滢馥筠柔竹霭凝晓欢霄枫芸菲寒伊亚宜可姬舒影荔枝思丽";
//   static String boyName = "伟刚勇毅俊峰强军平保东文辉力明永健世广志义兴良海山仁波宁贵福生龙元全国胜学祥才发武新利清飞彬富顺信子杰涛昌成康星光天达安岩中茂进林有坚和彪博诚先敬震振壮会思群豪心邦承乐绍功松善厚庆磊民友裕河哲江超浩亮政谦亨奇固之轮翰朗伯宏言若鸣朋斌梁栋维启克伦翔旭鹏泽晨辰士以建家致树炎德行时泰盛雄琛钧冠策腾楠榕风航弘";
//
//    static String [] bjlx = new String[]{"异常震动","断电告警","围栏越界","长期掉线","设防位移"};
//    @Test
//    void data() {
//        List<LcwsSbJbxx> list = new ArrayList<>();
//        Calendar calendar=Calendar.getInstance();
//        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
//        Random random = new Random();
//        for (int i=0;i<10000;i++){
//            LcwsSbJbxx bjxx = new LcwsSbJbxx();
//            bjxx.setId(SnowFlake.nextId());
//            bjxx.setSbzdbh(SnowFlake.nextId());
//            bjxx.setXzqhdm(xzqhdm[random.nextInt(xzqhdm.length)]);
//            bjxx.setCllx(cllx[random.nextInt(4)]);
//            bjxx.setSbdhhm(randomPhoneCode());
//            bjxx.setCzXm(randomName());
//            bjxx.setCphm(randomCarCode());
//            calendar.add(Calendar.DATE,random.nextInt(20));
//            bjxx.setFCjsj(format.format(calendar.getTime()));
//            list.add(bjxx);
//        }
//        lcwsSbJbxxImpl.saveBatch(list);
//
//        List<LcwsSbClzt> clztList = new ArrayList<>();
//        for(LcwsSbJbxx jbxx:list){
//            LcwsSbClzt clzt = new LcwsSbClzt();
//            clzt.setId(jbxx.getId());
//            clzt.setClzt(Math.random()>0.5?"0":"1");
//            clztList.add(clzt);
//        }
//        lcwsSbClztImpl.saveBatch(clztList);
//
//        List<LcwsSbBjxx> bjxxList = new ArrayList<>();
//        for(LcwsSbJbxx jbxx:list){
//            LcwsSbBjxx bjxx = new LcwsSbBjxx();
//            bjxx.setId(jbxx.getId());
//            bjxx.setBjlx(bjlx[random.nextInt(bjlx.length)]);
//            calendar.add(Calendar.DATE,random.nextInt(20));
//            bjxx.setFCjsj(format.format(calendar.getTime()));
//            bjxxList.add(bjxx);
//        }
//        lcwsSbBjxxImpl.saveBatch(bjxxList);
//    }
//
//
//    public static String randomPhoneCode(){
//        String first = String.valueOf(familyName1.charAt((int)(Math.random()*familyName1.length())));
//        if(Math.random()>0.5){
//            first+=girlName.charAt((int)(Math.random()*girlName.length()));
//            first+=girlName.charAt((int)(Math.random()*girlName.length()));
//        }else{
//            first+=boyName.charAt((int)(Math.random()*boyName.length()));
//            first+=boyName.charAt((int)(Math.random()*boyName.length()));
//        }
//        return first;
//    }
//
//    public static String randomName(){
//        StringBuffer stringBuffer = new StringBuffer();
//        for (int i=0;i<8;i++){
//            stringBuffer.append((int)(Math.random()*10));
//        }
//        return start[(int)(Math.random()*start.length)]+stringBuffer;
//    }
//
//
//    static String charUp = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
//    public static String randomCarCode(){
//        StringBuffer stringBuffer = new StringBuffer("川");
//        stringBuffer.append(charUp.charAt((int)(Math.random()*charUp.length())));
//        for (int i=0;i<6;i++){
//            stringBuffer.append((int)(Math.random()*10));
//        }
//        return stringBuffer.toString();
//    }
//
////    public static void main(String[] args) {
////        for (int i=0;i<100;i++)
////            System.out.println(randomCarCode());
////    }
//}
