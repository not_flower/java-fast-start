# javaFastStart

#### 介绍
分享一个针对java项目的shell脚本。经过几个项目的使用，确实很方便哈。
主要是springboot项目，linux环境。
非常方便的启动/关闭 java项目。滚动查看最新输出日志。以及打开debug模式，进行远程断点调试。


#### 3分钟使用教程
 **1，目录文件说明。** 
使用当前脚本之前，需要一个固定的目录结构(非必须，可自定义)。
![输入图片说明](https://images.gitee.com/uploads/images/2022/0430/110726_ada03a9e_8202700.png "屏幕截图.png")

- config目录下，可以放你本地的配置文件。一般开发过程中，有开发环境，测试环境，灰度环境，生产环境。各环境有不同的配置文件，先将配置文件存放在不同的环境中。发布的时候就不需要再去修改本地的配置文件，直接上传jar包即可。
- lib目录下，存放你项目依赖的jar包。我们在发布项目的时候，可以把依赖和项目打包到一个jar中。也可以把项目和依赖分离出来。这样发布的时候只需要上传业务jar(一般几百kb)，能够提高上传速度。当然，也有不方便的地方，就是加入你引入新的jar要及时更新到各环境中去。否则遗忘了，启动不起来，也挺麻烦的。我一般的做法是测试环境依赖分离，灰度环境和生产环境都是打到一起的。
- logs目录，很简单就是存放日志文件的目录。一般我们配置日志文件的时候都会指定日志的输出目录，每个日志文件大小也要手动指定。这样logs目录下会有许多日志文件，在滚动查看的时候也会不太方便。
- demo文件就是我分享的这个脚本文件
- demo-0.0.1-SNAPSHOT.jar 就是我用来演示这个脚本使用方式的简单工程。
- demo-0.0.1-SNAPSHOT.jar.back 这个文件就是我做的一个半包项目，它只有200多kb。一秒上云，非常方便。



 **2，脚本使用说明。** 

- 首先把config，lib，logs几个目录建好。将demo文件，和jar包上传到linux服务器。
- 当前目录下 输入命令 chmod +x demo 把执行权限赋给脚本文件
- 当前目录下 输入命令 ./demo -h 出现下图日志。说明可用

![输入图片说明](https://images.gitee.com/uploads/images/2022/0430/105635_16815836_8202700.png "屏幕截图.png")


```
         -s              启动服务::这个就是全包启动。针对依赖和项目打包到一起的情况。
         -sl             启动服务并打印日志::全包启动，并且启动同时，滚动输出日志。
         -ss             半包 启动服务::这个就是针对 依赖和项目不在一起的情况。
         -rr             半包 重启服务::重启，脚本会先找到项目的PID,然后kill -15,再执行启动命令
         -r              重启服务::重启，脚本会先找到项目的PID,然后kill -15,再执行启动命令
         -rl             重启服务并打印日志::先关闭服务，再启动，启动过程中滚动输出日志
         -c              关闭服务::找到项目pid然后 kill -15。
         -l              打印日志::找到项目日志目录下最新的日志文件，滚动输出
         -d              用debug模式启动服务 （全包启动）需要指定一个端口 ./demo -d 8080
                                在你的idea工具上配置这个端口。就能远程调试了。
         -dd             用debug模式启动服务 （半包启动）
         -h              查看帮助命令
```



 **3，脚本实践。** 
在实际应用中，你还需要到脚本文件中指定一下你的jar名称。

- 打开demo脚本，将变量 pro_name="demo-0.0.1-SNAPSHOT.jar" 改成你自己的 xxx-x-x.jar就行了。
- 你还可以把demo这个脚本的文件名，改成你想要的名字 xxx。这样你在使用的时候可以 ./xxx -h 就可以执行了。

 **4，扩展。** 

 **自定义日志输出目录** 
在使用过程中，你可能输出的日志目录和上述标准结构不同。也没有关系
在脚本中：

```
print_logs(){
	log_file=$(ls -lt ${path}/logs | grep .log | head -n 1 | awk '{print $9}')
	echo ${log_file}
        // ${path}/logs 修改这个值 比如 url="/usr/local/logs/${log_file}" 
        // 其中 ${log_file}是脚本在此目录下，找到的最新日志文件名称。不要去动
	url="${path}/logs/${log_file}" 
	echo ${url}
	tail -f ${url}
}
```

 **抽离依赖** 
在maven的pom文件里的 plugins 标签下。这样添加如下配置

```
<plugin>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-maven-plugin</artifactId>
    <configuration>
        <layout>ZIP</layout>
        <includes>
            <!-- 不含依赖 -->
            <include>
                <groupId>nothing</groupId>
                <artifactId>nothing</artifactId>
            </include>
        </includes>
    </configuration>
</plugin>
<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-dependency-plugin</artifactId>
    <executions>
        <execution>
            <id>copy-dependencies</id>
            <phase>package</phase>
            <goals>
                <goal>copy-dependencies</goal>
            </goals>
            <configuration>
                <type>jar</type>
                <includeTypes>jar</includeTypes>
                <includeScope>runtime</includeScope>
                <outputDirectory>
                    ${project.build.directory}/lib
                </outputDirectory>
            </configuration>
        </execution>
    </executions>
</plugin>
```

 **远程断点配置** ，IDEA编辑器，eclipse没配置过。百度一下吧

第一步

![输入图片说明](128727.jpg)


第二步

![输入图片说明](92865639.jpg)


第三步

![输入图片说明](09828978762.jpg)


最后点击

![输入图片说明](782376623.jpg)

你的远程服务需要以debug模式运行噢。